swaygrab
========

Swaygrab is a small, easy to use tool for taking screenshots under sway.

Usage is pretty simple:

    swaygrab active  # capture the currently active window.
    swaygrab region  # capture a manually select a region.
    swaygrab window  # capture a manually select a window.
    swaygrab output  # capture the currently active output.

The idea is to just bind this to sway key bindings, e.g.:

    bindsym Mod4+p       exec swaygrab active
    bindsym Mod4+Mod1+p  exec swaygrab
    bindsym Mod4+Shift+p exec swaygrab output
    bindsym Mod4+Ctrl+p  exec swaygrab window

See `man 1 swaygrab` for usage instructions.

Credit
------

Swaygrab project relies on `grim` and `slurp` to do the heavy lifting, and uses
`jq` to do most of the gluing.

Licence
-------

Swaygrab is licensed under the ISC licence. See LICENCE for details.
