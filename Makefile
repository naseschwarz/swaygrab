PREFIX?=/usr/local
BINDIR?=$(DESTDIR)$(PREFIX)/bin
MANDIR?=$(DESTDIR)$(PREFIX)/share/man

swaygrab.1: swaygrab.1.scd
	scdoc < $< > $@

all: swaygrab.1

clean:
	rm -rf swaygrab.1

install: all
	mkdir -p $(BINDIR) $(MANDIR)/man1
	install -m755 swaygrab $(BINDIR)/swaygrab
	install -m644 swaygrab.1 $(MANDIR)/man1/swaygrab.1

.PHONY: all clean install
